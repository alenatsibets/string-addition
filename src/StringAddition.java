import java.util.Scanner;

public class StringAddition {
    public static void main(String[] args) {
        String number1, number2;
        number1 = inputFromConsole();
        number2 = inputFromConsole();
        char[] number1ToArray = number1.toCharArray();
        char[] number2ToArray = number2.toCharArray();
        char[] sum = addition(number1ToArray, number2ToArray);
        display(sum);
    }
    public static String inputFromConsole() {
        String number;
        System.out.print("Enter a number: ");
        Scanner sc = new Scanner(System.in);
        number = sc.next();
        return number;
    }
    public static char[] addition(char[] number1, char[] number2){
        if(number1.length < number2.length){
            char[] temp = number1;
            number1 = number2;
            number2 = temp;
        }
        char[] sum = new char[number1.length];
        int carry = 0;
        for (int i = 0; i < number2.length; i++){
            char temp = (char) (number1[number1.length - 1 - i] + number2[number2.length - 1 - i] + carry - 48);
            carry = 0;
            if (temp > 57) {
                temp -= 10;
                carry = 1;
            }
            sum[i] = temp;
        }
        if (number1.length > number2.length) {
            System.arraycopy(number1, 0, sum, number2.length, number1.length - number2.length);
            if (carry == 1){
                for (int i = number2.length; i < sum.length; i++){
                    sum[i] = (char) (sum[i] + carry);
                    carry = 0;
                    if (sum[i] > 57) {
                        sum[i] -= 10;
                        carry = 1;
                    }
                }
            }
        }
        if (carry == 1){
            char[] newSum = new char[sum.length + 1];
            System.arraycopy(sum, 0, newSum, 0, sum.length);
            newSum[sum.length] = 49;
            return newSum;
        }
        return sum;
    }
    public static void display(char[] sum){
        for (int i = sum.length - 1; i >= 0; i--) {
            System.out.print(sum[i]);
        }
    }
}